/** @format */
import { AppRegistry, UIManager } from 'react-native';
import App from './src/app';
import {name as appName} from './app.json';

//Setting UIManager Flag in order to get LayoutAnimation work on android.
UIManager.setLayoutAnimationEnabledExperimental && 
UIManager.setLayoutAnimationEnabledExperimental(true);

AppRegistry.registerComponent(appName, () => App);
