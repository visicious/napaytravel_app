import React, {Component} from 'react';
import {ActivityIndicator, AsyncStorage, Platform, StyleSheet, Text, View, Button} from 'react-native';
//import OAuthManager from 'react-native-oauth';
import {Router, Scene} from 'react-native-router-flux';
import Authentication from './routes/Authentication';
import MapNapay from './routes/mapNapay';

/*const manager = new OAuthManager('firestackexample')
manager.configure({
  facebook: {
    client_id: '578478032621140',
    client_secret: '2c6e4dde7503c49a9c91f5294eeeb839'
  },
  google: {
    callback_url: `127.0.0.1:8081/google`,
    client_id: '391580961815-o4q1s0vuhq5ujmdhj6bed6tqrgug5j4l.apps.googleusercontent.com',
    client_secret: '5oRUDMk0Q2XNv9-Vf4-kO41E'
  }
});*/

//TODO: Add a kind of action with these pieces of code to kind redirect when clinking
//      in a button to obtain the data 
/*manager.authorize('google', {scopes: 'email,profile'})
.then(resp => console.log('Your users ID'))
.catch(err => console.log('There was an error'));

manager.authorize('facebook', {scopes: 'email,profile'})
.then(resp => console.log('Your users ID'))
.catch(err => console.log('There was an error'));*/

type Props = {};
export default class App extends Component<Props> {
  constructor() {
    super();
    this.state = { hasToken: false, isLoaded: false };
  }

  componentDidMount() {
    AsyncStorage.getItem('id_token').then((token) => {
      this.setState({ hasToken: token !== null, isLoaded: true })
    });
  }

  render() {
    if (!this.state.isLoaded) {
      return (
        <ActivityIndicator />
      )
    } else {
      return(
        <Router>
          <Scene key='root'>
            <Scene
              component={MapNapay}
              hideNavBar={true}
              key='MapNapay'
              title='Napay Travel'
            />
          </Scene>
        </Router>
      )
    }
  }
}