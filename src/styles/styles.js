import { StyleSheet,Dimensions } from 'react-native';

const { windowWidth, windowHeight } = Dimensions.get("window");
export default styles = StyleSheet.create({
  buttonText: {
    fontSize: 20,
    padding: 10,
    color:'#ffffff',
    textAlign: 'center'
  },
  buttonWrapper: {
    backgroundColor:'#fd5f53',
    marginBottom: 10,
    justifyContent: 'center',
    width: '96%'
  },
  secondaryButtonWrapper: {
    backgroundColor:'#17b057',
    marginBottom: 10,
    justifyContent: 'center',
    width: '96%'
  },
  cancelButtonWrapper: {
    backgroundColor:'#E93A2C',
    marginBottom: 10,
    justifyContent: 'center',
    width: '96%'
  },
  container: {
    alignItems: 'center',
    flex: 1,
    backgroundColor:'rgba(256, 256, 256, 0)',
    justifyContent: 'center'
  },
  scrollContainer: {
    alignItems: 'center',
  },
  form: {
    alignItems: 'center',
    width: Dimensions.get('window').width * 0.95
  },
  image: {
    margin: 10
  },
  imageLogo: {
    margin: 10,
    marginBottom: 25,
    width: Dimensions.get('window').width * 0.80,
    aspectRatio: 1.5, 
    resizeMode: 'contain'
  },
  inputText: {
    marginBottom: 10,
    padding: 10,
    width: Dimensions.get('window').width * 0.95,
    backgroundColor:'#f5f5f5'
  },
  pickerText: {
    width: '100%',
  },
  title: {
    fontSize: 40,
    margin: 10,
    paddingBottom: 10,
    textAlign: 'center',
    color:'#fd5f53'
  },
  homeBackgroundImage: {
    width: '100%', 
    height: '100%'
  },
});