import React, {Component} from 'react';
import {Alert, AsyncStorage, Image, ImageBackground, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../styles/styles';

type Props = {};
export default class Authentication extends Component<Props> {

  constructor() {
    super();
    this.state = { username: null, password: null, name:null, age:null, nationality:null };
  }

  userSignup() {
    if (!this.state.username || !this.state.password || !this.state.name || !this.state.age || !this.state.nationality) 
      return;

    fetch('http://www.napaytravel.com/api/v2/users/add_user', {
      method: 'POST',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        name: this.state.name,
        age: this.state.age,
        nationality: this.state.nationality,
        token:  'WrfsQG5ROF8kTuwFU1HBWVKngzNNBY7v+',
      })
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.saveItem('id_token', responseData.id_token),
      Alert.alert( 'Signup Success!', 'Welcome dear pathfinder! Add your discoverings in the form below'),
      Actions.MapNapay();
    })
    .done();
  }

  userLogin() {
    if (!this.state.username || !this.state.password) 
      return;

    fetch('http://www.napaytravel.com/api/v2/users/login', {
      method: 'POST',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        token:  'WrfsQG5ROF8kTuwFU1HBWVKngzNNBY7v+',
      })
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.saveItem('id_token', responseData.id_token),
      Alert.alert('Login Success!', 'Welcome back dear pathfinder! Add your discoverings in the form below'),
      Actions.MapNapay();
    })
    .done();
  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  render() {
    return (
      <ImageBackground
        source={require('../images/napaytravel-background.png')}
        imageStyle={{resizeMode: 'cover'}}
        style={styles.homeBackgroundImage}
      >
        <View style={styles.container}>
          <Image source={require('../images/napaytravel-logo.png')} style={styles.imageLogo}/>


          <View style={styles.form}>
            <TextInput
              editable={true}
              onChangeText={(name) => this.setState({name})}
              placeholder='Complete Name'
              ref='name'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.name}
            />

            <TextInput
              editable={true}
              onChangeText={(age) => this.setState({age})}
              placeholder='Age'
              ref='age'
              keyboardType = 'numeric'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.age}
            />

            <TextInput
              editable={true}
              onChangeText={(nationality) => this.setState({nationality})}
              placeholder='Nationality'
              ref='nationality'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.nationality}
            />

            <TextInput
              editable={true}
              onChangeText={(username) => this.setState({username})}
              placeholder='Username'
              ref='username'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.username}
            />

            <TextInput
              editable={true}
              onChangeText={(password) => this.setState({password})}
              placeholder='Password'
              ref='password'
              returnKeyType='next'
              secureTextEntry={true}
              style={styles.inputText}
              value={this.state.password}
            />
          

            <TouchableOpacity style={styles.secondaryButtonWrapper} onPress={this.userLogin.bind(this)}>
              <Text style={styles.buttonText}> Log In </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonWrapper} onPress={this.userSignup.bind(this)}>
              <Text style={styles.buttonText}> Sign Up </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}