import React, {Component} from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  View,
  Animated,
  Image,
  Dimensions,
  Button,
  TouchableHighlight, 
  TouchableOpacity, 
  TouchableNativeFeedback, 
  TouchableWithoutFeedback,
  Picker,
  Platform,
  ScrollView,
} from "react-native";
import { 
  SearchBar,
} from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView from "react-native-maps";
import {Actions} from 'react-native-router-flux';


const Images = [
  { uri: "https://www.tierra-inca.com/album/photos/104/10444.jpg" },
  { uri: "http://www.go2peru.travel/destinos/large/Aqp_may06_0147.jpg" },
  { uri: "http://4.bp.blogspot.com/-EHE1j8bDWWw/U1m9k_wDUMI/AAAAAAAAAIs/LTMdhLo9x78/s1600/LA+BENITA+EN+DOCUMENTAL+SOBRE+PICANTERIAS+EN+EL+PERU.png" },
  { uri: "https://q-fa.bstatic.com/data/xphoto/1920x810/244/24444190.jpg" }
]

const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;

type Props = {};
export default class MapNapay extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      fichaDetailHeight: 0, //For scrollview correct sizing
      showDonationPopUp: true,
      showPointDescription: false, //change false when finishing editing styles in scrollview
      showPointFilters: false,
      markers: [],
      filters: [],
      /*markers: [
        {
          coordinate: {
            latitude: -16.3953101,
            longitude: -71.5368205,
          },
          title: "Monasterio Santa Catalina",
          description: "Vasto convento dominico construido en el siglo XVI; ahora abierto al público con visitas guiadas y libres",
          image: Images[0],
        },
        {
          coordinate: {
            latitude: -16.3997674,
            longitude: -71.5361492,
          },
          title: "Claustros de la Compañia",
          description: "Casa de culto preservada que data del siglo XVIII, adecuado para la fotografía, el paseo y la contemplacion de arquitectura del siglo XVIII",
          image: Images[1],
        },
        {
          coordinate: {
            latitude: -16.400172,
            longitude: -71.5365029,
          },
          title: "La Benita: Picantería",
          description: "Modelo del restaurante tradicional de Arequipa: la picantería; de los cuales se encuentran ya muy pocos en estado adecuado",
          image: Images[2],
        },
        {
          coordinate: {
            latitude: -16.4006517,
            longitude: -71.5424316,
          },
          title: "Hostal Las Mercedes",
          description: "Hostal y camping adecuado para los viajeros de paso y mochileros",
          image: Images[3],
        },
      ],*/
      region: {
        latitude: -16.399072,
        longitude: -71.536575,
        latitudeDelta: 0.164564554807,
        longitudeDelta: 0.16542727081,

        //Initial Values: very good ones
        /*latitudeDelta: 0.04864195044303443,
        longitudeDelta: 0.040142817690068,*/
      },
      napayCardsArray: [],
      actualMarker: {
          coordinate: {
            latitude: -16.400172,
            longitude: -71.5365029,
          },
          title: "La Benita: Picantería",
          description: "Modelo del restaurante tradicional de Arequipa: la picantería; de los cuales se encuentran ya muy pocos en estado adecuado",
          image: Images[0],
        },
      disabled:false,
    };

    this.index = 0;
    this.animatedValue = new Animated.Value(0);
  }

  addMore = () =>
  {
      this.animatedValue.setValue(0);
   
      let newlyAddedValue = { index: this.index }
   
      this.setState({ disabled: true, napayCardsArray: [ ...this.state.napayCardsArray, newlyAddedValue ] }, () =>
      {
          Animated.timing(
              this.animatedValue,
              {
                  toValue: 1,
                  duration: 500,
                  useNativeDriver: true
              }
          ).start(() =>
          {
              this.index = this.index + 1;
              this.setState({ disabled: false });
          }); 
      });              
  }

  renderPointDescription = (pointIndex) => {
    this.setState({  actualMarker: this.state.markers[pointIndex], showPointDescription: true });
  }

  async fetchInitialPoints() {
    fetch('http://www.napaytravel.com/api/v2/point_places/get_heap_points', {
      method: 'GET',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    })
    .then((response) => response.json())
    .then((responseData) => {
      responseData.map((napayPoint) => {     
        let eachNapayPoint = {
          coordinate: {
            latitude: parseFloat(napayPoint.latitude),
            longitude: parseFloat(napayPoint.longitude),
          },
          title: napayPoint.name.trim(),
          description: napayPoint.description.trim(),
          image: Images[0],
          n_category: napayPoint.n_category.trim(),
          n_type: napayPoint.n_type.trim(),
          n_subtype: napayPoint.n_subtype.trim(),
          score: napayPoint.score.trim(),
          food_tast_score: napayPoint.food_tast_score.trim(),
          higine_score: napayPoint.higine_score.trim(),
          nice_atmosphere_score: napayPoint.nice_atmosphere_score.trim(),
          quality_service_score: napayPoint.quality_service_score.trim(),
          thief_danger_score: napayPoint.thief_danger_score.trim(),
          car_accident_danger_score: napayPoint.car_accident_danger_score.trim(),
          fall_danger_score: napayPoint.fall_danger_score.trim(),
          drowning_danger_score: napayPoint.drowning_danger_score.trim(),
          fraud_scam_danger_score: napayPoint.fraud_scam_danger_score.trim(),
          building_structural_danger_score: napayPoint.building_structural_danger_score.trim(),
          prices: napayPoint.prices.trim(),
          in_place_time: napayPoint.in_place_time.trim(),
          waiting_time: napayPoint.waiting_time.trim(),
          time_table: napayPoint.time_table.trim(),
          attention_days: napayPoint.attention_days.trim(),
          schedule: napayPoint.schedule.trim(),
          entry_time: napayPoint.entry_time.trim(),
          departure_time: napayPoint.departure_time.trim(),
          languages: napayPoint.languages.trim(),
          change_type: napayPoint.change_type.trim(),
          free_services: napayPoint.free_services.trim(),
          charge_services: napayPoint.charge_services.trim(),
          currency_change: napayPoint.currency_change.trim(),
          objects_allowed_mandatory: napayPoint.objects_allowed_mandatory.trim(),
          objects_disallowed_mandatory: napayPoint.objects_disallowed_mandatory.trim(),
          objects_allowed_suggestion: napayPoint.objects_allowed_suggestion.trim(),
          objects_disallowed_suggestion: napayPoint.objects_disallowed_suggestion.trim(),
          concurrence: napayPoint.concurrence.trim(),
          place_size: napayPoint.place_size.trim(),
          eat_rest_proximities: napayPoint.eat_rest_proximities.trim(),
          other_proximities: napayPoint.other_proximities.trim(),
          contact: napayPoint.contact.trim(),
        }

        this.setState({
          markers: [...this.state.markers, eachNapayPoint]
        });
      });

      
    })
    .done();
  }

  componentWillMount() {
    this.index = 0;
    this.animation = new Animated.Value(0);
    this.fetchPointFilters();
    this.fetchInitialPoints();
  }

  componentDidMount() {
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here
    this.animation.addListener(({ value }) => {
      let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
      if (index >= this.state.markers.length) {
        index = this.state.markers.length - 1;
      }
      if (index <= 0) {
        index = 0;
      }

      clearTimeout(this.regionTimeout);
      this.regionTimeout = setTimeout(() => {
        if (this.index !== index) {
          this.index = index;
          const { coordinate } = this.state.markers[index];
          this.map.animateToRegion(
            {
              ...coordinate,
              latitudeDelta: this.state.region.latitudeDelta,
              longitudeDelta: this.state.region.longitudeDelta,
            },
            350
          );
        }
      }, 10);
    });
  }

  markerClick(){
    console.log("Marker was clicked");
  }

  calculateFichaDetailResize = (contentWidth, contentHeight) => {
    this.setState({fichaDetailHeight: contentHeight});
  }

  renderPointFilters = () => {
    this.setState({showPointFilters: true});
  }

  renderpointCategoryComponent = (categoryName) => {
    switch(categoryName) {
      case 'places_visit':
        return <Text>Lugares para Visitar</Text>;
      case 'restaurants_food':
        return <Text>Restaurantes y Comida</Text>;
      case 'agencies':
        return <Text>Agencias</Text>;
      case 'shop':
        return <Text>Tiendas</Text>;
      case 'lodgment':
        return <Text>Hospedaje</Text>;
      case 'groceries':
        return <Text>Abarrotes</Text>;
      case 'terminals':
        return <Text>Terminales</Text>;
      case 'money':
        return <Text>Dinero</Text>;
      case 'night_activities':
        return <Text>Entretenimiento Nocturno</Text>;
      default:
        return null;
    }
  }

  async fetchPointFilters() {
    fetch('http://napaytravel.com/api/v2/point_places/get_point_filters/n_category', {
      method: 'GET',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    })
    .then((response) => response.json())
    .then((responseData) => {
      let n_categoryFilters = {
        "n_category": responseData,
      }
      
      this.setState({
        filters: [...this.state.filters, n_categoryFilters]
      });
    })
    .done();

    fetch('http://napaytravel.com/api/v2/point_places/get_point_filters/n_type', {
      method: 'GET',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    })
    .then((response) => response.json())
    .then((responseData) => {
      let n_typeFilters = {
        "n_type": responseData,
      }
      
      this.setState({
        filters: [...this.state.filters, n_typeFilters]
      });
      
    })
    .done();

    fetch('http://napaytravel.com/api/v2/point_places/get_point_filters/n_subtype', {
      method: 'GET',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    })
    .then((response) => response.json())
    .then((responseData) => {
      let n_subtypeFilters = {
        "n_subtype": responseData,
      }
      
      this.setState({
        filters: [...this.state.filters, n_subtypeFilters]
      });
      
    })
    .done();
  }

  render() {
    const animationValue = this.animatedValue.interpolate(
        {
            inputRange: [ 0, 1 ],
            outputRange: [ -59, 0 ]
        });

    let newArray = this.state.napayCardsArray.map(( item, key ) =>
      {
        if(( key ) == this.index)
        {
            return(
                <Animated.View key = { key } style = {[ styles.viewHolder, { opacity: this.animatedValue, transform: [{ translateY: animationValue }] }]}>
                    <Text style = { styles.text }>Row { item.index }</Text>
                </Animated.View>
            );
        }
        else
        {
            return(
                <View key = { key } style = { styles.viewHolder }>
                    <Text style = { styles.text }>Row { item.index }</Text>
                </View>
            );
        }
      });

    const interpolations = this.state.markers.map((marker, index) => {
      const inputRange = [
        (index - 1) * CARD_WIDTH,
        index * CARD_WIDTH,
        ((index + 1) * CARD_WIDTH),
      ];
      const scale = this.animation.interpolate({
        inputRange,
        outputRange: [1, 2.5, 1],
        extrapolate: "clamp",
      });
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: "clamp",
      });
      return { scale, opacity };
    });

    const enableScrollFichaDetail = this.state.fichaDetailHeight > height;

    return (
      <View style={styles.container}>
        <MapView
          ref={map => this.map = map}
          initialRegion={this.state.region}
          style={styles.container}
        >
          {this.state.markers.map((marker, index) => {
            const scaleStyle = {
              transform: [
                {
                  scale: interpolations[index].scale,
                },
              ],
            };
            const opacityStyle = {
              opacity: interpolations[index].opacity,
            };
            return (
              <MapView.Marker key={index} coordinate={marker.coordinate} title={marker.title} onCalloutPress={() => {this.addMore; this.renderPointDescription(index);}} >
                <Animated.View style={[styles.markerWrap, opacityStyle]}>
                  <Animated.View style={[styles.ring, scaleStyle]} />
                  <View style={styles.marker} />
                </Animated.View>
              </MapView.Marker>
            );
          })}
        </MapView>
        <View style={styles.centerBar}>
          
          <TouchableHighlight style={styles.filterButton} onPress={() => { Alert.alert('En progreso', 'Disculpe los inconvenientes, estamos trabajando para resolver el problema'); }} underlayColor="white">
            <View style={styles.filterButtonContainer}>
              <Text style={styles.filterButtonText}>Filtros</Text>
              <Icon name="caret-down" style={styles.filterArrow} size={13} color="#717477" />
            </View>
          </TouchableHighlight>
          
          <SearchBar
              placeholder='¿A donde va a ir?' 
              containerStyle= {styles.searchBar}/>
          
          { this.state.showPointFilters ? (
            <TouchableHighlight style={styles.filterButton} onPress={() => { this.renderPointFilters(); }} underlayColor="white">
              <View style={styles.filterButtonContainer}>
                <Text style={styles.filterButtonText}>Filtros</Text>
                <Icon name="caret-down" style={styles.filterArrow} size={13} color="#717477" />
              </View>
            </TouchableHighlight>
          ) : null}
        </View>


        { this.state.showDonationPopUp ? (
          <View style={styles.centerBarPopUp}>
           <TouchableHighlight  onPress={() => { this.setState({ showDonationPopUp: false }); }} underlayColor="white" style={styles.pointDetailClose}>
              <View >
               <Text style={styles.pointDetailCloseText}>X</Text>
              </View>
            </TouchableHighlight>
            <Text style={styles.pointDetailName}>Querido amigo viajero, necesitamos tu ayuda para mejorar este prototipo! Puedes encontrar los detalles en napaytravel.com/crowfunding </Text>
          </View>
        ) : null}

        <Animated.ScrollView
          horizontal
          scrollEventThrottle={1}
          showsHorizontalScrollIndicator={false}
          snapToInterval={CARD_WIDTH}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    x: this.animation,
                  },
                },
              },
            ],
            { useNativeDriver: true }
          )}
          style={styles.scrollView}
          contentContainerStyle={styles.endPadding}
        >
          {this.state.markers.map((marker, index) => (
            <TouchableOpacity onPress={() => this.renderPointDescription(index)}>
              <View style={styles.card} key={index}>
                <Image
                  source={marker.image}
                  style={styles.cardImage}
                  resizeMode="cover"
                />
                <View style={styles.textContent}>
                  <Text numberOfLines={1} style={styles.cardtitle}>{marker.title}</Text>
                  <Text numberOfLines={2} style={styles.cardDescription}>
                    {marker.description}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          ))}
        </Animated.ScrollView>
        { this.state.showPointDescription ? (
            
            <View style = {{ position: "absolute", backgroundColor: "rgba(52,52,52,0.6)", display: "flex", left: 0, top: 0, width: "100%", height: height}}>
            <TouchableOpacity onPress={() => { this.setState({ showPointDescription: false }); }} style={{height: height, width: "30%",  position: "absolute", display: "flex", left: 0, top: 0}}>
            </TouchableOpacity>
            <ScrollView style = {{ position: "absolute", backgroundColor: "#fff", display: "flex", right: 0, top: 0, width: "70%", height: height}}  scrollEnabled={true} onContentSizeChange={this.calculateFichaDetailResize} >
              <View style = {{height: height, padding: "1%" }}>
                <TouchableHighlight  onPress={() => { this.setState({ showPointDescription: false }); }} underlayColor="white" style={styles.pointDetailClose}>
                  <View >
                    <Text style={styles.pointDetailCloseText}>X</Text>
                  </View>
                </TouchableHighlight>
                <Image
                  source={this.state.actualMarker.image}
                  style={{width: "100%",height: "20%"}}
                  resizeMode="cover"
                />
                <View style={{ flex: 3 }}>
                  <Text style={styles.pointDetailName}>{this.state.actualMarker.title}</Text>
                  <Text style={styles.pointDetailDescription}>
                    {this.state.actualMarker.description}
                  </Text>
                  <Text style={styles.pointProperty}>
                    <Text style={{fontWeight: "bold"}}>Categoría: </Text>
                    { this.renderpointCategoryComponent(this.state.actualMarker.n_category)}
                  </Text>
                  <Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Tipo: </Text><Text>{this.state.actualMarker.n_type}</Text></Text>
                  {this.state.actualMarker.n_subtype != "" && this.state.actualMarker.n_subtype != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Subtipo: </Text><Text>{this.state.actualMarker.n_subtype}</Text></Text>) : null }
                  <View style={styles.pointBlockDivider}/>
                  <View style={styles.pointBlockProperties}>
                  <Text style={styles.pointPropertyTitle}>Calificaciones</Text>
                  {this.state.actualMarker.score != "" && this.state.actualMarker.score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Calificación General: </Text><Text>{this.state.actualMarker.score}</Text></Text>) : null }
                  {this.state.actualMarker.food_tast_score != "" && this.state.actualMarker.food_tast_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Sabor de la comida: </Text><Text>{this.state.actualMarker.food_tast_score}</Text></Text>) : null }
                  {this.state.actualMarker.higine_score != "" && this.state.actualMarker.higine_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Higiene: </Text><Text>{this.state.actualMarker.higine_score}</Text></Text>) : null }
                  {this.state.actualMarker.nice_atmosphere_score != "" && this.state.actualMarker.nice_atmosphere_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Ambiente Agradable: </Text><Text>{this.state.actualMarker.nice_atmosphere_score}</Text></Text>) : null }
                  {this.state.actualMarker.quality_service_score != "" && this.state.actualMarker.quality_service_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Calidad de servicio: </Text><Text>{this.state.actualMarker.quality_service_score}</Text></Text>) : null }
                  </View>
                  <View style={styles.pointBlockDivider}/>
                  <View style={styles.pointBlockProperties}>
                  <Text numberOfLines={1} style={styles.pointPropertyTitle}>Peligrosidad</Text>
                  {this.state.actualMarker.thief_danger_score != "" && this.state.actualMarker.thief_danger_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Delincuencia: </Text><Text>{this.state.actualMarker.thief_danger_score}</Text></Text>) : null}
                  {this.state.actualMarker.car_accident_danger_score != "" && this.state.actualMarker.car_accident_danger_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Accidentes vehiculares: </Text><Text>{this.state.actualMarker.car_accident_danger_score}</Text></Text>) : null}
                  {this.state.actualMarker.fall_danger_score != "" && this.state.actualMarker.fall_danger_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Accidentes de montaña: </Text><Text>{this.state.actualMarker.fall_danger_score}</Text></Text>) : null}
                  {this.state.actualMarker.drowning_danger_score != "" && this.state.actualMarker.drowning_danger_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Ahogamiento: </Text><Text>{this.state.actualMarker.drowning_danger_score}</Text></Text>) : null}
                  {this.state.actualMarker.fraud_scam_danger_score != "" && this.state.actualMarker.fraud_scam_danger_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Hurto y estafa: </Text><Text>{this.state.actualMarker.fraud_scam_danger_score}</Text></Text>) : null}
                  {this.state.actualMarker.building_structural_danger_score != "" && this.state.actualMarker.building_structural_danger_score != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Estado de la estructura: </Text><Text>{this.state.actualMarker.building_structural_danger_score}</Text></Text>) : null}
                  </View>
                  <View style={styles.pointBlockDivider}/>
                  {this.state.actualMarker.prices != "" && this.state.actualMarker.prices != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Precios: </Text><Text>{this.state.actualMarker.prices}</Text></Text>) : null }
                  {this.state.actualMarker.in_place_time != "" && this.state.actualMarker.in_place_time != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Estadía/Duración: </Text><Text>{this.state.actualMarker.in_place_time}</Text></Text>) : null }
                  {this.state.actualMarker.waiting_time != "" && this.state.actualMarker.waiting_time != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Tiempo de espera: </Text><Text>{this.state.actualMarker.waiting_time}</Text></Text>) : null }
                  {this.state.actualMarker.time_table != "" && this.state.actualMarker.time_table != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Tabla de duración: </Text><Text>{this.state.actualMarker.time_table}</Text></Text>) : null }
                  {this.state.actualMarker.attention_days != "" && this.state.actualMarker.attention_days != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Días de Atención: </Text><Text>{this.state.actualMarker.attention_days}</Text></Text>) : null }
                  {this.state.actualMarker.schedule != "" && this.state.actualMarker.schedule != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Horario: </Text><Text>{this.state.actualMarker.schedule}</Text></Text>) : null }
                  {this.state.actualMarker.entry_time != "" && this.state.actualMarker.entry_time != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Máxima Hora de Entrada: </Text><Text>{this.state.actualMarker.entry_time}</Text></Text>) : null }
                  {this.state.actualMarker.departure_time != "" && this.state.actualMarker.departure_time != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Máxima Hora de Salida: </Text><Text>{this.state.actualMarker.departure_time}</Text></Text>) : null }
                  {this.state.actualMarker.languages != "" && this.state.actualMarker.languages != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Idiomas: </Text><Text>{this.state.actualMarker.languages}</Text></Text>) : null }
                  {this.state.actualMarker.change_type != "" && this.state.actualMarker.change_type != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Tipo de cambio: </Text><Text>{this.state.actualMarker.change_type}</Text></Text>) : null }
                  {this.state.actualMarker.free_services != "" && this.state.actualMarker.free_services != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Servicios incluidos: </Text><Text>{this.state.actualMarker.free_services}</Text></Text>) : null }
                  {this.state.actualMarker.charge_services != "" && this.state.actualMarker.charge_services != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Productos/servicios de pago que ofrece el establecimiento: </Text><Text>{this.state.actualMarker.charge_services}</Text></Text>) : null }
                  {this.state.actualMarker.objects_allowed_mandatory != "" && this.state.actualMarker.objects_allowed_mandatory != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>¿Que se debe llevar?: </Text><Text>{this.state.actualMarker.objects_allowed_mandatory}</Text></Text>) : null }
                  {this.state.actualMarker.objects_disallowed_mandatory != "" && this.state.actualMarker.objects_disallowed_mandatory != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>¿Que no se debe llevar?: </Text><Text>{this.state.actualMarker.objects_disallowed_mandatory}</Text></Text>) : null }
                  {this.state.actualMarker.objects_allowed_suggestion != "" && this.state.actualMarker.objects_allowed_suggestion != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>¿Que se debería llevar?: </Text><Text>{this.state.actualMarker.objects_allowed_suggestion}</Text></Text>) : null }
                  {this.state.actualMarker.objects_disallowed_suggestion != "" && this.state.actualMarker.objects_disallowed_suggestion != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>¿Que no es necesario llevar?: </Text><Text>{this.state.actualMarker.objects_disallowed_suggestion}</Text></Text>) : null }
                  {this.state.actualMarker.concurrence != "" && this.state.actualMarker.concurrence != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Concurrencia: </Text><Text>{this.state.actualMarker.concurrence}</Text></Text>) : null }
                  {this.state.actualMarker.place_size != "" && this.state.actualMarker.place_size != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Tamaño: </Text><Text>{this.state.actualMarker.place_size}</Text></Text>) : null }
                  {this.state.actualMarker.eat_rest_proximities != "" && this.state.actualMarker.eat_rest_proximities != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Proximidades (para comer y descansar): </Text><Text>{this.state.actualMarker.eat_rest_proximities}</Text></Text>) : null }
                  {this.state.actualMarker.other_proximities != "" && this.state.actualMarker.other_proximities != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Otras Proximidades: </Text><Text>{this.state.actualMarker.other_proximities}</Text></Text>) : null }
                  {this.state.actualMarker.contact != "" && this.state.actualMarker.contact != "." ? (<Text style={styles.pointProperty}><Text style={{fontWeight: "bold"}}>Información de contacto: </Text><Text>{this.state.actualMarker.contact}</Text></Text>) : null }

                </View>
              </View>
            </ScrollView>
            </View>
          ) : null
        }
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    position: "absolute",
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  filterButton: {
    width: "14%",
    height: 58,
    backgroundColor: "#fff",
  },
  filterButtonContainer:{
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: "center",
    paddingLeft: 9,
    width: "100%",
    backgroundColor: "#3fc256",
  },
  filterArrow: {
    width: "15%",
    backgroundColor: "#3fc256",
  },
  filterButtonText: {
    width: "85%",
    fontSize: 16,
    color: "#fff",
    backgroundColor: "#3fc256",
  },
  searchBar: {
    width: "82%",
    height: 58,
  },
  centerBar: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: "absolute",
    top: width * 0.17,
    left: width * 0.03,
    right: width * 0.03,
    width: "90%",
    marginLeft: "2%",
    marginRight: "2%",
    backgroundColor: "#fff",
  },
  centerBarPopUp: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: "absolute",
    top: width * 0.17,
    left: width * 0.03,
    right: width * 0.03,
    width: "90%",
    marginLeft: "2%",
    marginRight: "2%",
    backgroundColor: "#3fc256",
    zIndex: 5,
  },
  card: {
    padding: 10,
    elevation: 2,
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },
  textContent: {
    flex: 1,
  },
  pointDetailName: {
    fontSize: 20,
    marginTop: 5,
    color: "#fd5f53",
    fontWeight: "bold",
  },
  pointDetailDescription: {
    fontSize: 12,
    padding: 6,
    borderColor: "#888",
    marginBottom: 10,
    textAlign: 'justify',
    backgroundColor: '#efefef',
    borderWidth: 1,
    color: "#358e9b",
  },
  pointBlockProperties: {
    textAlign: 'justify',
  },
  pointPropertyTitle: {
    fontSize: 15,
    color: "#fd5f53",
    fontWeight: "bold",
  },
  pointProperty: {
    fontSize: 12,
    marginTop: 5,
    textAlign: 'justify',
    color: "#358e9b",
  },
  pointBlockDivider: {
    borderBottomColor: '#fd5f53',
    borderBottomWidth: 1,
    width: "100%",
    marginTop: 6,
    marginBottom: 6,
  },
  pointDetailClose: {
    height: 32,
    width: 32,
    left: 0,
    top: 0,    
    backgroundColor: "#ed2939",
    paddingLeft: 9,
    position: "absolute",
    zIndex: 1,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ed2939',
  },
  pointDetailCloseText: {
    color:"#ffffff",
    fontSize: 20,
    fontWeight: "bold",
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold",
    color: "#fd5f53",
  },
  cardDescription: {
    fontSize: 12,
    color: "#444",
    color: "#358e9b",
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: "rgba(130,4,150, 0.3)",
    position: "absolute",
    borderWidth: 1,
    borderColor: "rgba(130,4,150, 0.5)",
  },
});